require 'spec_helper'

require 'mailscan/cli'

def fixture(name)
  File.join(File.dirname(__FILE__), 'fixtures', "#{name}.eml")
end

describe "Multipart" do
  it "should list parts as array" do
    m = Mail.read(fixture('multipart_parts'))
    expected = ['text/plain; charset=UTF-8', 'text/html; charset=UTF-8']
    extract_parts(m.parts.first).map(&:content_type).should == expected
    extract_parts(m).map(&:content_type).should == expected
  end

  it "should select first text when both html" do
    m = Mail.read(fixture('multipart_parts'))
    all = extract_parts(m)
    find_scanable(all).content_type.should == 'text/plain; charset=UTF-8'
  end

  it "should select html when no text" do
    m = Mail.read(fixture('multipart_parts'))
    all = extract_parts(m)
    html = all.last
    find_scanable([html]).content_type.should == 'text/html; charset=UTF-8'
  end

  it "should show error on unknown input"  do
    IO.popen('bin/mailscan', 'w+') do |pipe|
      pipe.write('abc')
      pipe.close_write
      pipe.read
    end.should =~ /ERROR/
  end

  it "should parse email"  do
    res = IO.popen('bin/mailscan', 'w+') do |pipe|
      pipe.write File.read(fixture('multipart_parts'))
      pipe.close_write
      pipe.read
    end

    m = Mail.new(res)
    m.content_type.should == 'text/plain; charset=UTF-8'
    m.body.should =~ /additional/
    m.parts.size.should == 0
  end

  it "should parse html only" do
    res = IO.popen('bin/mailscan', 'w+') do |pipe|
      pipe.write File.read(fixture('html_only'))
      pipe.close_write
      pipe.read
    end

    m = Mail.new(res)
    m.content_type.should == 'text/html; charset=UTF-8'
    m.body.should =~ /!DOCTYPE/
    m.parts.size.should == 0
  end

  it "should handle short email" do
    m = Mail.read(fixture('short_and_empty'))
    all = extract_parts(m)
    plain = all.last
    find_scanable([plain]).content_type.should == 'text/plain'
    find_scanable([plain]).body.should =~ %r{https://github.com/cucumber}
  end

  it "should show content" do

    res = IO.popen('bin/mailscan', 'w+') do |pipe|
      pipe.write File.read(fixture('unknown'))
      pipe.close_write
      pipe.read
    end
    res.should == 'abc'
  end
end
