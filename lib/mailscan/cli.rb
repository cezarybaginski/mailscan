require 'mailscan'

module Mailscan
  class Cli
    #TODO: not tested
    def self.run(args)
      input = $stdin.read

      # get file type
      type = IO.popen('file -', 'w+') do |pipe|
        pipe.write input
        pipe.close_write
        pipe.read
      end

      mime_type, result = process(type, input)

      #format result as email
      unless result.empty?
        outmail = Mail.new
        outmail.content_type = mime_type
        outmail.body = result
        $stdout.write(outmail.to_s)
      else
        $stdout.write('ERROR')
      end
    end
  end
end
