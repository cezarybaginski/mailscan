require 'nokogiri'

require "mailscan/version"

module Mailscan
  # Your code goes here...
end

require 'mail'

def tidy(html)
  #TODO:
  html
end

#TODO: not tested
def process(type, input)

  if type =~ /HTML document/
    begin
      input = input.
        gsub("\u2019","'").
        gsub("\u2013","-").
        gsub("\u2022",'*').
        gsub("\u201C",'"').
        gsub("\u201D",'"').
        gsub("\u2014",'--').
        gsub("\u2026",'...').
        gsub("\u200B",'').
        gsub("\u00A0",' ').
        gsub("\u00A0",' ').
        gsub("\u00A9",'(c)').
        encode('us-ascii').
        gsub(/\xBD/n,'&xbd;').
        gsub(/\xA0/n,'&xa0;').
        encode('utf-8')
    rescue Encoding::UndefinedConversionError => e
      return ['text/html charset=UTF-8', "<html> <body> <p> <pre>mailscan error: #{e.to_s}</pre> <a href='http://localhost/error'>#{e.to_s}</a> </p></body> </html>"]
    end

    html = input
    return ['text/html charset=UTF-8', cleanup_html(html)]
  else
    m = Mail.new(input)
    parts = extract_parts(m)
    part = find_scanable(parts)
    if part
      outmail = Mail.new
      if [/text/, /html/].any? {|type| part.content_type =~ type }
        return [type, part.body.to_s]
      end
    end
  end
  ["",""]
end

#TODO: not tested
def cleanup_html(html)
  Nokogiri::HTML(tidy(html.encode('utf-8'))).tap do |nokogiri|
    %w(span img).each do |tag|
      nokogiri.search("//#{tag}").each do |tag|
        tag.replace(tag.children.to_s)
      end
    end
  end.to_s
end



def extract_parts(mail)
  return [mail.part.first] unless mail.multipart?
  parts = []
  mail.parts.each do |part|
    if part.parts.empty?
      if part.content_type =~ /text|html/
        parts << part
      end
    else
      parts += extract_parts(part)
    end
  end
  parts
end

def find_scanable(parts)
  parts.detect do |part|
    if part.content_type =~ /text/
      part
    elsif part.content_type =~ /html/
      part
    else
      nil
    end
  end
end
