# -*- encoding: utf-8 -*-
$:.push File.expand_path("../lib", __FILE__)
require "mailscan/version"

Gem::Specification.new do |s|
  s.name        = "mailscan"
  s.version     = Mailscan::VERSION
  s.authors     = ["Cezary Baginski"]
  s.email       = ["cezary.baginski@gmail.com"]
  s.homepage    = ""
  s.summary     = %q{Scans and fixes emails for urlscan}
  s.description = %q{Cleans up mail for urlscan to work properly}

  s.rubyforge_project = "mailscan"

  s.files         = `git ls-files`.split("\n")
  s.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  s.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  s.require_paths = ["lib"]

  s.add_runtime_dependency "nokogiri"
  s.add_runtime_dependency "mail"

  s.add_development_dependency "bundler"
end
